import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

import { getDatabase,onValue,ref,get,set,child,update,remove}
from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";

import { getStorage, ref as refS,uploadBytes,getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";

 const firebaseConfig = {
    apiKey: "AIzaSyBcEoJ5cGy-S7cuulPT0HAYwHNQU3c0OF4",
    authDomain: "proyecto2-6e7ac.firebaseapp.com",
    projectId: "proyecto2-6e7ac",
    storageBucket: "proyecto2-6e7ac.appspot.com",
    messagingSenderId: "397410071230",
    appId: "1:397410071230:web:437688cd24ad482ce564ef"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const auth = getAuth();

  document.addEventListener("DOMContentLoaded", function(){

    const loginForm = document.getElementById('loginForm');
    const btnEnviar = document.getElementById('btnEnviar');

    loginForm.addEventListener("submit", function(event){
      event.preventDefault();
    });

    btnEnviar.addEventListener("click", login);

    function login(){
      const email = document.getElementById('txtEmail').value;
      const password = document.getElementById('txtPassword').value;

      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          const user = userCredential.user;
          alert("Inicio de sesión exitoso. Usuario: " + user.displayName);
          window.location.href = '../html/menu.html';
        })
        .catch((error) => {
          const errorCode = error.code;
          const errorMessage = error.message;
          alert("Error al iniciar sesión: " + errorMessage);
        });

    }
  })

